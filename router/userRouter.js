const router = require('express').Router();
const user = require('../controller/userController.js');
const contact = require('../controller/contactController');

router.get('/login', (req,res) => res.render('auth/login', {title: "Login"}));
router.post('/login', user.login);
router.get('/register', (req,res) => res.render('register', {title: "Register"}));
router.post('/register', user.doRegister);

// router.get('/send', (req,res) => res.render('sendContact'));
router.post('/pengiriman', contact.sending);

//bisa diakses jika sudah login dan punya token
const restrict = require('../middlewares/restrict');
router.get('/dashboard', restrict, user.dashboard);

router.get('/logout', restrict, user.logout);

//skills
router.get('/skills', restrict, user.allSkill);
router.get('/skills/:id', restrict, user.detailSkill);
router.get('/skills-add', restrict, (req,res) => res.render('user/skills_add', {title: "Skills"}));
router.post('/skills', restrict, user.addSkill);
router.put('/skills/:id', restrict, user.updateSkill);
router.delete('/skills/:id', restrict, user.deleteSkill);

router.get('/skills-edit/:id', restrict, user.editSkill);

//about
router.get('/about', restrict, user.allAbout);
router.get('/about/:id', restrict, user.detailAbout);
router.post('/about', restrict, user.addAbout);
router.put('/about/:id', restrict, user.updateAbout);
router.delete('/about/:id', restrict, user.deleteAbout);

router.get('/about-edit/:id', restrict, user.editAbout);

//education
router.get('/edu', restrict, user.allEducation);
router.get('/edu/:id', restrict, user.detailEducation);
router.get('/edu-add', restrict, (req,res) => res.render('user/edu_add', {title: "Education"}));
router.post('/edu', restrict, user.addEducation);
router.put('/edu/:id', restrict, user.updateEducation);
router.delete('/edu/:id', restrict, user.deleteEducation);

router.get('/edu-edit/:id', restrict, user.editEducation);

//portofolio
router.get('/porto', restrict, user.allPortfolio);
router.get('/porto/:id', restrict, user.detailPortfolio);
router.get('/porto-add', restrict, (req,res) => res.render('user/porto_add', {title: "Portfolio"}));
router.post('/porto', restrict, user.addPortfolio);
router.put('/porto/:id', restrict, user.updatePortfolio);
router.delete('/porto/:id', restrict, user.deletePortfolio);

router.get('/porto-edit/:id', restrict, user.editPortfolio);

//contact atau inbox
router.get('/allContact', restrict, contact.allEmail);
router.delete('/deleteContact/:id', restrict, contact.delete)

module.exports = router;