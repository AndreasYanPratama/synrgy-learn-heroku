const router = require("express").Router();

const userRouter = require('./userRouter');

// router.use('/', (req,res) => res.render('index') );
router.use('/user', userRouter);
router.get('/', (req,res) => res.render('sendContact'));
module.exports = router;