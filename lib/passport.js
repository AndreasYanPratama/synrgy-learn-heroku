const passport = require('passport');
//menggunakan Local Strategy saja
const LocalStrategy = require('passport-local').Strategy;
const { Strategy : JwtStrategy, ExtractJwt } = require('passport-jwt');
const { User } = require('../models');

/* Passport JWT Options */
// const options = {
//     // untuk mengekstrak JWT dari request, dan mengambil token-nya dari header yang bernama Authorization
//     jwtFromRequest : ExtractJwt.fromHeader('Authorization'),
//     // jwtFromRequest: ExtractJWT.fromAuthHeader('Authorization'),
//     // jwt: ExtractJwt.fromAuthHeader,

//     /* Harus sama seperti dengan apa yang kita masukkan sebagai parameter kedua dari jwt.sign di User Model.
//         Inilah yang kita pakai untuk memverifikasi apakah tokennya dibuat oleh sistem kita */
//     secretOrKey : 'Ini rahasia ga boleh disebar-sebar' ,
// }

async function authenticate(username, password, done) {
    try {
        const user = await User.authenticate({ username, password});
        console.log(user);
        return done(null,user);
    } catch (err) {
        return done(null, false, { message: err.message });
    }
}

passport.use(
    new LocalStrategy({ usernameField: 'username', passwordField: 'password'}, authenticate)
)

// passport.use(new JwtStrategy(options, async (payload, done) => {
//     // payload adalah hasil terjemahan JWT, sesuai dengan apa yang kita masukkan di parameter pertama dari jwt.sign
//     User.findByPk(payload.id)
//     .then(user => done(null, user))
//     .catch(err => done(err, false))
// }))

passport.serializeUser(
    (user, done) => done(null, user.id)
)

passport.deserializeUser(
    async(id, done) => done(null, await User.findByPk(id))
)

module.exports = passport;