const { Contact } = require('../models');
// const passport = require('passport');
const passport = require('../lib/passport');

module.exports = {

    sending: (req, res, next) => {
        Contact.sending(req.body).then(() => {
            res.json({
                'status': 200,
                'message': 'success sending'
            })
        }).catch(err => next(err))
    },

    allEmail: (req, res) => {
        const title = "Inbox";
        Contact.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then( send => {
            // res.json({
            //     'status': 200,
            //     'data' : send
            // })
            res.render('user/inbox', {title, send})
        }).catch(err => next(err))
    },

    delete: (req, res) => {
        const id = req.params.id;
        Contact.destroy({
            where: {
                id: id
            }
        }).then(() => {
            // res.json({
            //     'status': 200,
            //     'message': 'success deleting'
            // })
            res.send('/user/allContact')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    }
};