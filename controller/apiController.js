const { User, Skill, About, Education, Portfolio } = require('../models');
// const passport = require('passport');
const passport = require('../lib/passport');
const expressSession = require('express-session');
var http = require('http');

module.exports = {

    allSkill: (req, res) => {
        const title = "Skills";
        Skill.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then( skills => {
            res.json({
                'status': 200,
                'data' : skills
            })
            // res.render('user/skills', {title, skills});
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    detailSkill: (req, res) => {
        const id = req.params.id
        Skill.findOne({
            where: {
                id: id
            }
        }).then( send => {
            res.json({
                'status': 200,
                'data': send
            })
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    addSkill: (req, res) => {
        Skill.addSkill(req.body).then(() => {
            res.json({
                'status': 200,
                'message': 'success adding'
            })
            // res.redirect('skills')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    updateSkill: (req, res) => {
        const id = req.params.id;
        Skill.update({
            title: req.body.title,
            icon: req.body.icon
        }, {
            where:{
                id: id
            }
        }).then(send => {
            res.json({
                'status': 200,
                'message': 'success updating'
            })
            // console.log(send) 
            // res.send('/user/skills')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    deleteSkill: (req, res) => {
        const id = req.params.id;
        Skill.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
            // res.send('/user/skills')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    //=====About
    allAbout: (req, res) => {
        const title = "About"
        About.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then( about => {
            res.json({
                'status': 200,
                'data' : about
            })
            // res.render('user/about',{title,about})
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    detailAbout: (req, res) => {
        const title = "About"
        const id = req.params.id
        About.findOne({
            where: {
                id: id
            }
        }).then( about => {
            res.json({
                'status': 200,
                'data': about
            })
            // res.render('user/about_edit',{title, about})
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    addAbout: (req, res) => {
        const {title, desc} = req.body;
        About.create({
            title,
            desc
        }).then( send => {
            res.json({
                'status': 200,
                'message': 'success adding',
                'data' : send
            })
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    updateAbout: (req, res) => {
        const id = req.params.id;
        const {title, desc} = req.body;
        About.update({
            title: req.body.title,
            desc: req.body.desc
        }, {
            where:{
                id: id
                // id: req.body.id
            }
        }).then( send => {
            res.json({
                'status': 200,
                'message': 'success updating'
            })
            // console.log(send) 
            // res.send('/user/about')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    
    deleteAbout: (req, res) => {
        const id = req.params.id;
        About.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    //=====Education

    allEducation: (req, res) => {
        const title = "Education"
        Education.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then( education => {
            res.json({
                'status': 200,
                'data' : education
            })
            // res.render('user/edu', {education, title})
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    detailEducation: (req, res) => {
        const id = req.params.id
        Education.findOne({
            where: {
                id: id
            }
        }).then( send => {
            res.json({
                'status': 200,
                'data': send
            })
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    addEducation: (req, res) => {
        const {institute, title, program, periode} = req.body;
        Education.create({
            institute,
            title,
            program,
            periode
        }).then( send => {
            res.json({
                'status': 200,
                'message': 'success adding'
            })
            // res.redirect('edu')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    editEducation: (req, res) => {
        const title = "Edit Education"
        const id = req.params.id
        Education.findOne({
            where: {
                id: id
            }
        }).then(edu => {
            res.render('user/edu_edit', {title, edu})
        }).catch(err => {
            res.json({
                'status': 500,
                'message': 'kesalahan server'
            })
            // console.log(err)
        })
    },
    updateEducation: (req, res) => {
        const id = req.params.id;
        Education.update({
            institute: req.body.institute,
            title: req.body.title,
            program: req.body.program,
            periode: req.body.periode
        }, {
            where:{
                id: id
            }
        }).then( send => {
            res.json({
                'status': 200,
                'message': 'success updating'
            })
            // console.log(send) 
            // res.send('/user/edu')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    deleteEducation: (req, res) => {
        const id = req.params.id;
        Education.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
            // res.send('/user/edu')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    //=====Portfolio

    allPortfolio: (req, res) => {
        const title = "Portofolio"
        Portfolio.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then( porto => {
            res.json({
                'status': 200,
                'data' : porto
            })
            // res.render('user/porto',{title, porto})
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    detailPortfolio: (req, res) => {
        const id = req.params.id
        Portfolio.findOne({
            where: {
                id: id
            }
        }).then( send => {
            res.json({
                'status': 200,
                'data': send
            })
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    addPortfolio: (req, res) => {
        const {title, type, desc, link} = req.body;
        Portfolio.create({
            title,
            type,
            desc,
            link
        }).then( send => {
            res.json({
                'status': 200,
                'message': 'success adding'
            })
            // res.redirect('porto')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },
    editPortfolio: (req, res) => {
        const title = "Edit Portfolio"
        const id = req.params.id
        Portfolio.findOne({
            where: {
                id: id
            }
        }).then(porto => {
            res.render('user/porto_edit', {title, porto})
        }).catch(err => {
            res.json({
                'status': 500,
                'message': 'kesalahan server'
            })
            // console.log(err)
        })
    },
    updatePortfolio: (req, res) => {
        const id = req.params.id;
        Portfolio.update({
            title: req.body.title,
            type: req.body.type,
            desc: req.body.desc,
            link: req.body.link
        }, {
            where:{
                id: id
            }
        }).then( send => {
            res.json({
                'status': 200,
                'message': 'success updating'
            })
            // console.log(send) 
            // res.send('/user/porto')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    deletePortfolio: (req, res) => {
        const id = req.params.id;
        Portfolio.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
            // res.send('/user/porto')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    },

    //=====Inbox Message
    sending: (req, res, next) => {
        Contact.sending(req.body).then(() => {
            res.json({
                'status': 200,
                'message': 'success sending'
            })
        }).catch(err => next(err))
    },

    allEmail: (req, res) => {
        const title = "Inbox";
        Contact.findAll({
            order: [
                ['id', 'ASC'],
            ],
        }).then( send => {
            res.json({
                'status': 200,
                'data' : send
            })
            // res.render('user/inbox', {title, send})
        }).catch(err => next(err))
    },

    delete: (req, res) => {
        const id = req.params.id;
        Contact.destroy({
            where: {
                id: id
            }
        }).then(() => {
            res.json({
                'status': 200,
                'message': 'success deleting'
            })
            // res.send('/user/allContact')
        }).catch(err => {
            res.json({
                'status':400,
                'message': err
            })
        })
    }

}