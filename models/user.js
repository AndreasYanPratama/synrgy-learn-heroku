'use strict';
const {
  Model
} = require('sequelize');

//pertama, lita import bcrypt
const bcrypt = require('bcrypt');
// memakai jwt
const jwt = require('jsonwebtoken');

module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    //method utk melakukan enkripsi
    static #encrypt = (password) => bcrypt.hashSync(password, 10)
    //method register
    static register = ({ username, password }) => {
      const encryptedPassword = this.#encrypt(password)
      /*
        #encrypt dari static method
        encryptedPassword akan sama dengan string
        hasil enkripsi password dari method #encrypt
      */
      return this.create({ username, password: encryptedPassword })
    }
    /* register tidak dikenai JWT*/
    /*===========================================*/
    
    //method .compareSync digunakan untuk mencocokkan plaintext(form) dg hash(db)
    //method utk melakukan enkripsi
    checkPassword = password => bcrypt.compareSync(password, this.password);

    /* method untuk membuat JWT */
    generateToken = () => {
      //jangan memasukkan password ke dalam payload
      const payload = {
        id: this.id,
        username: this.username
      }
      // Rahasia ini nantinya kita pakai untuk memverifikasi apakah token ini benar-benar berasal dari aplikasi kita
      const rahasia = 'Ini rahasia ga boleh disebar-sebar';
      // Membuat token dari data-data diatas
      const token = jwt.sign(payload, rahasia);
      return token;
    }

    // method Authenticate, untuk login
    static authenticate = async ({ username, password }) => {
      try {
        const user = await this.findOne({ where: { username }});
        if (!user) return Promise.reject("User not found");

        const isPasswordValid = user.checkPassword(password);
        if (!isPasswordValid) return Promise.reject("Wrong Password");
        
        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error)
      }
    }

    /*===========================================*/
    // static changePass = ({ username, password }) => {
    //   const encryptedPassword = this.#encrypt(password)
    //   /*
    //     #encrypt dari static method
    //     encryptedPassword akan sama dengan string
    //     hasil enkripsi password dari method #encrypt
    //   */
    //   // return this.create({ username, password: encryptedPassword })
    //   return this.update
    // }

  };
  User.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};