'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Skill extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }

    static addSkill = ({ title, icon }) => {
      /*
        #encrypt dari static method
        encryptedPassword akan sama dengan string
        hasil enkripsi password dari method #encrypt
      */
      return this.create({ title, icon })
    }

  };
  Skill.init({
    title: DataTypes.STRING,
    icon: DataTypes.TEXT
  }, {
    sequelize,
    modelName: 'Skill',
  });
  return Skill;
};