// const cors = require('cors');
const express = require('express');
const app = express();
// app.use(cors());
const session = require('express-session');
const flash = require('express-flash');
// const port = 8000;
const port = process.env.PORT || 8000;
// var whitelist = ['http://localhost:8000', 'http://localhost:8080'];
// var corsOptions = {
//     origin: function (origin, callback) { if (whitelist.indexOf(origin) !== -1) { callback(null, true); } else { callback(null, false); } }, methods: ['GET', 'PUT', 'POST', 'DELETE', 'OPTIONS'], optionsSuccessStatus: 200, credentials: true, allowedHeaders: ['Content-Type', 'Authorization', 'X-Requested-With', 'device-remember-token', 'Access-Control-Allow-Origin', 'Origin', 'Accept'] }; app.use(cors(corsOptions)); 
     

    // Pertama, setting request body parser
    // (Ingat! Body parser harus ditaruh paling atas)
    const cookie = require('cookie-parser');
    app.use(cookie());

    app.use(express.urlencoded({ extended: true}));
    app.use(express.json());

    // app.set('view engine','ejs');

    // Kedua, setting session handler
    app.use(session({
        secret: 'Buat ini jadi rahasia',
        resave: false,
        saveUninitialized: false
    }))

// Ketiga, setting passport
// (sebelum router dan view engine)
const passport = require('./lib/passport');
    app.use(passport.initialize());
    app.use(passport.session());

    // Keempat, setting flash
    app.use(flash());

    // Kelima, setting view engine
    app.set('view engine', 'ejs');

    // Keenam, setting router
    const router = require('./router');
    app.use(router)
app.listen(port, () => {
        console.log(`server running in port http://localhost:${port}`);
    })