const passport = require('../lib/passport')

// module.exports = passport.authenticate('jwt', {session: false})

// apa jika sudah dg JWT, tidak boleh memakai module dibawh ini

module.exports = (req, res, next) => {
    // Bila request berasal dari user yang terautentikasi,
    // maka kita akan lanjut menjalankan handler berikutnya
    if (req.isAuthenticated()) return next()
    // Bila tidak, kita akan redirect ke halaman login
    res.redirect('/user/login')
    // res.redirect('login')
}

// module.exports = (req, res, next) => {
//     const authHeader = req.headers.authorization;

//     if (authHeader) {
//         const token = authHeader.split(' ')[1];

//         jwt.verify(token, accessTokenSecret, (err, user) => {
//             if (err) {
//                 return res.sendStatus(403);
//             }

//             req.user = user;
//             next();
//         });
//     } else {
//         res.sendStatus(401);
//     }
// };

